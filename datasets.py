import numpy as np
import scipy as sp
import scipy.io
from sklearn import preprocessing

def CIV(file=None,scale=True,trim=False):
    X,Y={},{}
    if file:
        files=[file]
    else:
        files=files=['Caltech101','ImageNet','VOC2007']
    for file in files:
        data=sp.io.loadmat('data/VLSC/{}.mat'.format(file))['data']
        x,y= data[:,:-1],data[:,-1]-1
        x=x/np.max(x,0)
        if trim:
            cols = np.sum(x, 0) > 0
            x = x[:, cols]
        if scale:
            x=preprocessing.scale(x)
        X[file]=np.copy(x)
        Y[file]=np.copy(y)
    return X,Y

def CIVSplits(sF,tF):
    split=np.load('data/VLSC/{}_{}_splits.npy'.format(sF,tF),allow_pickle=True).item()
    return split['trainS'],split['trainT'],split['testS'],split['testT']

def mnistUsps(scale=True,trim=False):
    data = np.load('data/mnist_usps/mnist_vs_usps.npy', allow_pickle=True).item()
    Xs, Xt, Ys, Yt = data['X_src'], data['X_tar'], data['Y_src'], data['Y_tar']
    if trim:
        Xs = Xs[:, np.sum(Xs, 0) > 0]
        Xt = Xt[:, np.sum(Xt, 0) > 0]
    if scale:
        Xs=preprocessing.scale(Xs)
        Xt=preprocessing.scale(Xt)
    X = {'mnist':Xs, 'usps':Xt}
    Y = {'mnist':Ys, 'usps':Yt}
    return X,Y

def mnistUspsSplits(source,target):
    split = np.load('data/mnist_usps/' + source + '_vs_' + target + '_splits.npy', allow_pickle=True).item()
    return split['trainS'],split['trainT'],split['testS'],split['testT']

def caltechOffice(dataset, file=None, scale=True,trim=False):
    if file:
        files=[file]
    else:
        files = ['amazon', 'caltech', 'dslr', 'webcam']
    X, Y = {},{}
    for f in files:
        if dataset == 'decaf':
            data = sp.io.loadmat('data/decaf6/{}_decaf.mat'.format(f))
            x=data['feas']
        elif dataset=='surf':
            data = sp.io.loadmat('data/SURF_VOC/{}_SURF_L10.mat'.format(f))
            x=data['fts']

        y = data['labels']-1
        if trim:
            cols=np.sum(x,0)>0
            x=x[:,cols]
        if scale:
            x=preprocessing.scale(x)
        X[f]=np.copy(x)
        Y[f]=np.copy(y)
    return X, Y

def caltechOFficeSplits(sF,tF):
    split = sp.io.loadmat(
        'data/DataSplitsOfficeCaltech/SameCategory_{}-{}_20RandomTrials_10Categories.mat'.format(sF,tF))
    source = [split['train']['source'][0, 0][0][i].flatten() - 1 for i in range(20)]
    target = [split['train']['target'][0, 0][0][i].flatten() - 1 for i in range(20)]
    test_source = [split['test']['source'][0, 0][0][i].flatten() - 1 for i in range(20)]
    test_target = [split['test']['target'][0, 0][0][i].flatten() - 1 for i in range(20)]
    return source,target,test_source,test_target