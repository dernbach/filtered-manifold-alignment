import numpy as np
from sklearn.metrics import pairwise

class MA:
    def __init__(self,embDim=10,alpha=1):
        self.embDim=embDim
        self.alpha=alpha

    @staticmethod
    def nnGraph(X,neighbors,kernel='cosine',weighted=True,retD=False):
        V=pairwise.pairwise_kernels(X,X,metric=kernel,n_jobs=-1)
        inds = np.argsort(V, 1)
        L = np.zeros(V.shape)
        for i in range(len(L)):
            if weighted:
                L[i, inds[i, -(neighbors + 1):]] = -V[i, inds[i, -(neighbors + 1):]]
                L[inds[i, -(neighbors + 1):], i] = -V[i, inds[i, -(neighbors + 1):]]
            else:
                L[i, inds[i, -(neighbors+1):]] = -1
                L[inds[i, -(neighbors+1):], i] = -1
        diag = -L.sum(1)
        L = L + diag * np.eye(len(L))
        #d = L.diagonal().reshape(len(L), 1)
        return L

    def align(self,X,Y,neighbors=2):
        #Build Graphs
        L=[self.alpha*self.nnGraph(x,neighbors) for x in X]
        D = [l.diagonal().reshape(len(l), 1) for l in L]
        disqrt = 1.0 / np.sqrt(np.maximum(np.vstack(D),1))



        #Construct Joining Graph
        source_Nodes = len(X[0])
        target_Nodes = len(X[1])
        jointL=np.block([[L[0],np.zeros((source_Nodes,target_Nodes))],[np.zeros((target_Nodes,source_Nodes)),L[1]]])
        edges=[]
        for sind,s in zip(*Y[0]):
            for tind,t in zip(*Y[1]):
                if s == t:
                    jointL[sind, tind + source_Nodes]=-1
                    jointL[tind + source_Nodes,sind] = -1
                    jointL[sind,sind]+=1
                    jointL[tind+source_Nodes,tind+source_Nodes]+=1
        U,S,V=np.linalg.svd(disqrt*jointL*disqrt.T)
        emb=U[:,-self.embDim-1:-1]
        return emb[:source_Nodes],emb[-target_Nodes:]