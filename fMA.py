import numpy as np
from sklearn.metrics import pairwise

eps=1e-8

class FMA:
    def __init__(self,sourceDim,embDim,linear,alpha):
        self.sourceDim=sourceDim
        self.embDim=embDim
        self.linear=linear
        self.alpha=alpha

    @staticmethod
    def nnGraph(X,neighbors,kernel='cosine',weighted=True,retD=False):
        V=pairwise.pairwise_kernels(X,X,metric=kernel,n_jobs=-1)
        inds = np.argsort(V, 1)
        L = np.zeros(V.shape)
        for i in range(len(L)):
            if weighted:
                L[i, inds[i, -(neighbors + 1):]] = -V[i, inds[i, -(neighbors + 1):]]
                L[inds[i, -(neighbors + 1):], i] = -V[i, inds[i, -(neighbors + 1):]]
            else:
                L[i, inds[i, -(neighbors+1):]] = -1
                L[inds[i, -(neighbors+1):], i] = -1
        diag = -L.sum(1)
        L = L + diag * np.eye(len(L))
        return L

    def align(self,X,Y,L=None,neighbors=2):
        """
        X=[X[i]], X = samples x features
        Y=[Y1[i]], Y = [(ind, class)]
        L=[L[i]] if precomputed
        """
        #Build Graphs
        if not L:
            L=[self.alpha*self.nnGraph(x,neighbors) for x in X]
        D = [l.diagonal().reshape(len(l), 1) for l in L]

        # Calculate Eigenvectors
        U, S, T = [], [], []
        for i in range(len(L)):
            if self.linear:
                up, sp, vp = np.linalg.svd(X[i].T.dot(np.diag(D[i].flatten())).dot(X[i]))
                z = sp > eps
                T.append((1. / np.sqrt(sp[z])) * up[:, z])
                u, s, v = np.linalg.svd(T[-1].T.dot(X[i].T.dot(L[i]).dot(X[i]).dot(T[-1])))
            else:
                u, s, v = np.linalg.svd(np.diag(1. / np.sqrt(D[i]).flatten()).dot(L[i]).dot(
                    np.diag(1. / np.sqrt(D[i]).flatten())))
            U.append(u)
            S.append(s)

        #Construct Joining Graph
        source_Nodes = len(X[0])
        target_Nodes = len(X[1])
        features = X[0].shape[1]
        edges=[]
        for sind,s in zip(*Y[0]):
            for tind,t in zip(*Y[1]):
                if s == t:
                    edges.append((sind, tind + source_Nodes))
        A = np.zeros((source_Nodes + target_Nodes, len(edges)))
        for e in range(len(edges)):
            if self.linear:
                A[edges[e], e] = [1, -1]
            else:
                A[edges[e], e] = [1. / np.sqrt(D[0][edges[e][0]][0]),
                                  -1. / np.sqrt(D[1][edges[e][1] - source_Nodes][0])]
        if self.linear:
            jointX = np.block(
                [[X[0], np.zeros((X[0].shape[0], X[1].shape[1]))], [np.zeros((X[1].shape[0], X[0].shape[1])), X[1]]])
            jointT = np.block(
                [[T[0], np.zeros((T[0].shape[0], T[1].shape[1]))], [np.zeros((T[1].shape[0], T[0].shape[1])), T[1]]])
            A = jointT.T.dot(jointX.T).dot(A)

        #Learn Joint Embedding
        jointU, jointS = self.svdUpdate(U[0], U[1], S[0], S[1], A, self.sourceDim)
        if self.linear:
            offset = np.sum(jointS < eps)
            if offset > 0:
                transform = jointT.dot(jointU[:, -self.embDim - offset:-offset].dot(
                    np.diag(1. / np.sqrt(jointS[-self.embDim - offset:-offset]))))
            else:
                transform = jointT.dot(jointU[:, -self.embDim:].dot(np.diag(1. / np.sqrt(jointS[-self.embDim:]))))
            sEmb = (1. / np.sqrt(D[0])) * X[0].dot(transform[:X[0].shape[1]])
            tEmb = (1. / np.sqrt(D[1])) * X[1].dot(transform[X[0].shape[1]:])
        else:

            sEmb = (1. / np.sqrt(jointS[-self.embDim - 1:-1])) * jointU[:source_Nodes, -self.embDim - 1:-1] * (
                        1. / np.sqrt(D[0]))
            tEmb = (1. / np.sqrt(jointS[-self.embDim - 1:-1])) * jointU[source_Nodes:, -self.embDim - 1:-1] * (
                        1. / np.sqrt(D[1]))
            """
            sEmb = jointU[:source_Nodes, -self.embDim - 1:-1] * (
                    1. / np.sqrt(D[0]))
            tEmb = jointU[source_Nodes:, -self.embDim - 1:-1] * (
                    1. / np.sqrt(D[1]))
            """

        self.sEmb=sEmb
        self.tEmb=tEmb
        return sEmb,tEmb

    def fit_predict(self,Xs,Ys,Xt,Yt):
        return

    def svdUpdate(self, U1, U2, S1, S2, A, rank):
        U = np.block([[U1[:, -rank:], np.zeros((U1.shape[0], U2[:, -rank:].shape[1]))],
                      [np.zeros((U2.shape[0], U1[:, -rank:].shape[1])), U2[:, -rank:]]])
        UtA = U.T.dot(A)
        s = np.diag(np.hstack([S1[-rank:], S2[-rank:]]))
        block = s + UtA.dot(UtA.T)
        Up, Sp, Vp = np.linalg.svd(block)

        Up = U.dot(Up)
        return Up, Sp