import numpy as np
from sklearn.linear_model import LogisticRegression
import datasets
import time
from fMA import FMA
from CORAL import CORAL
from meda import MEDA
from gfk import GFK
from MA import MA
from suba import SemiSubspaceAlignedClassifier
eps=1e-8

######################################################
#############   Experiment Parameters   ##############
######################################################
experiment = 'caltech-office' # 'MU' 'CIV' 'caltech-office'
dataset = 'decaf' # 'surf' 'decaf'
method = 'FMA' # 'FMA' 'CORAL' 'MEDA' 'GFK' 'MA' 'SSA'
scaleF = True #Zscore Features
embDim = 40 # >0           //Used for All methods but CORAL

linear = False # True False True->Feature Based
neighbors = 12 # min 1
alpha = 0.2 # >0   Weights nearest neighbor edges
srcDim = int(embDim/2) #At least Emb Dim/2

kernel = 'rbf' # 'rbf' 'linear' 'primal'    //Used for MEDA
######################################################
######################################################
######################################################

if method=='FMA':
    da=FMA(srcDim,embDim,linear,alpha)
elif method=='CORAL':
    da=CORAL()
elif method=='MEDA':
    da=MEDA(kernel_type=kernel, dim=embDim)
elif method=='GFK':
    da=GFK(dim=embDim)
elif method=='MA':
    da=MA(embDim,alpha)
elif method=='SSA':
    da=SemiSubspaceAlignedClassifier(subspace_dim=embDim)

#Load Data
if experiment=='MU':
    domains=['mnist','usps']
    X,Y=datasets.mnistUsps(scaleF)
elif experiment=='CIV':
    domains=['ImageNet','VOC2007']#'Caltech101',
    X,Y=datasets.CIV()
elif experiment=='caltech-office':
    domains=['amazon','caltech','dslr','webcam']
    X,Y=datasets.caltechOffice(dataset)

acc_total=0.0
timers=[]
for source in domains:
    for target in domains:
        if source==target:
            continue
        if target=="Caltech101":
            continue

        print(source,"-->",target)
        Xs,Xt,Ys,Yt = X[source], X[target], Y[source], Y[target]
        if experiment=='MU':
            trainS, trainT, testS, testT = datasets.mnistUspsSplits(source,target)
        if experiment=='CIV':
            trainS, trainT, testS, testT = datasets.CIVSplits(source,target)
        elif experiment=='caltech-office':
            trainS, trainT, testS, testT = datasets.caltechOFficeSplits(source,target)

        acc = []
        for i in range(len(trainS)):
            trialTime = time.time()
            if method=='FMA' or method=='MA':
                Ysrc = (trainS[i], Ys[trainS[i]])
                Ytar = (trainT[i], Yt[trainT[i]])
                #fma = FMA(srcDim, embDim, linear)
                sEmb,tEmb = da.align([Xs,Xt],[Ysrc,Ytar],neighbors=neighbors)

                clf = LogisticRegression(solver='lbfgs', multi_class='multinomial').fit(sEmb[Ysrc[0]], Ysrc[1])
                acc.append(clf.score(tEmb[testT[i]], Yt[testT[i]]))
            elif method=='SSA':
                u=np.vstack([trainT[i],Yt[trainT[i]].flatten()]).T
                u=u.astype(int)
                da.fit(Xs[trainS[i]],Ys[trainS[i]].flatten(),Xt,u)
                acc.append(da.score(Xt[testT[i]],Yt[testT[i]].flatten()))
            else:
                if i>0: break #Unsupervised Methods don't have splits
                res = da.fit_predict(Xs, Ys, Xt, Yt)
                acc.append(res[0])
            trialTime = time.time() - trialTime
            timers.append(trialTime)
            print('Split',i,":",acc[-1],"Time:",trialTime)
            #np.save('timere',timers)

        print("Accuracy:",'{}+{}'.format(np.mean(acc), np.std(acc)))
        print()
        acc_total+=np.mean(acc)
acc_total=acc_total/(len(domains)*(len(domains)-1))
print('Total Accuracy:',acc_total)