# Semi-Supervised Filtered Manifold Alignment

This code can be run to reproduce the results found the Semi-Supervised Filtered Manifold Alignment Paper.


### Prerequisites
python 3.7.5

Numpy 1.17.4

Sklearn 0.21.3

Scipy 1.3.2

###Acknowledgements
The source code for GFK, MEDA, and CORAL were adapted from: https://github.com/jindongwang/transferlearning

The source code for SSA was adapted from: https://github.com/wmkouw/libTLDA

###Data Preparation
The datasets for office-Caltech, MNIST-USPS, and Caltech-ImageNet-Voc are included. All datasets were downloaded from: https://github.com/jindongwang/transferlearning/blob/master/data/dataset.md

### Running

Edit parameters within runExperiment.py

```
######################################################
experiment = 'caltech-office'
dataset = 'decaf'
method = 'FMA'
scaleF = True
embDim = 40

linear = True
neighbors = 12
alpha = 0.2
srcDim = int(embDim/2)

kernel = 'rbf'
######################################################
```

And run

```
python runExperiment.py
```

## General Parameters
Select which dataset to load from office-Caltech, mnist-usps, and caltech-imagenet-voc
```
experiment : 'caltech-office' | 'MU' | 'CIV'
```
In the case of office-Caltech, choose which between surf and decaf features
```
dataset = 'surf' | 'decaf'
```
Select the alignment algorithm
```
method = 'MEDA' | 'GFK' | 'CORAL' | 'SSA' | 'MA' | 'FMA'
```
Z-Score Features
```
scaleF = True | False
```
Choose the embedding dimension of the algorithm
```
embDim = [1,...)
```


##FMA Parameters
Choose whether to use Instance-level (False) or Feature-Level (True) FMA
```
linear = False | True
```
Select the number of neighbors in the knn graph for each domain
```
neighbors = [1,...)
```
Select edge weighting for knn graph
```
alpha = (0.0,...)
```
Select initial embedding dimesnion for each domain before aligning
```
srcDim = [embDim/2,...)
```

##MEDA Parameters
Select similarity kernel
```
kernel = 'rbf' | 'linear' | 'primal'
```